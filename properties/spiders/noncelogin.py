# -*- coding: utf-8 -*-
import scrapy
from properties.items import PropertiesItem
from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose, Join
from urllib.parse import urljoin
import datetime
import socket
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.http import FormRequest, Request


class NonceLoginSpider(CrawlSpider):
    name = 'noncelogin'
    allowed_domains = ['web']
    # start_urls = ['http://web:9312/dynamic']
    # Start with a login request
    def start_requests(self):
        return [Request("http://web:9312/dynamic/nonce", callback=self.parse_welcome)]

    rules = (
        Rule(LinkExtractor(restrict_xpaths='//*[contains(@class, "next")]')),
        Rule(LinkExtractor(restrict_xpaths='//*[@itemprop="url"]'), callback="parse_item")
    )

    # Post welcome page's first form with the given user/pass
    def parse_welcome(self, response):
        return FormRequest.from_response(response, formdata={"user": "user", "pass": "pass"})

    def parse_item(self, response):
        # Create the loader using the response
        l = ItemLoader(item=PropertiesItem(), response=response)

        # Load fields using XPath expressions
        l.add_xpath("title", '//*[@itemprop="name"][1]/text()', MapCompose(str.strip, str.title))
        l.add_xpath("price", '//*[@itemprop="price"][1]/text()', MapCompose(lambda i: i.replace(',', ''), float),
                    re='[.0-9]+')
        l.add_xpath("description", '//*[@itemprop="description"][1]/text()', MapCompose(str.strip), Join())
        l.add_xpath("address", '//*[@itemtype="http://schema.org/Place"][1]/text()', MapCompose(str.strip))
        l.add_xpath("image_urls", '//*[@itemprop="image"][1]/@src', MapCompose(lambda i: urljoin(response.url, i)))

        # Housekeeping fields
        l.add_value('url', response.url)
        l.add_value('project', self.settings.get('BOT_NAME'))
        l.add_value('spider', self.name)
        l.add_value('server', socket.gethostname())
        l.add_value('date', datetime.datetime.now())

        return l.load_item()
