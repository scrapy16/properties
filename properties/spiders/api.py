# -*- coding: utf-8 -*-
import scrapy
from properties.items import PropertiesItem
from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose, Join
from scrapy.http import Request
from urllib.parse import urljoin
import datetime
import socket
import json

class ApiSpider(scrapy.Spider):
    name = 'api'
    allowed_domains = ['web']
    start_urls = ['http://web:9312/properties/api.json']

    def parse(self, response):
        base_url = 'http://web:9312/properties/'
        js = json.loads(response.body)
        for item in js:
            id = item['id']
            url = base_url + "property_{}.html".format(id)
            title = item['title']
            yield Request(url, meta={"title": title}, callback=self.parse_item)

        # # Get the next index URLs and yield Requests
        # next_selector = response.xpath('//*[contains(@class, "next")]//@href')
        # for url in next_selector.extract():
        #     yield Request(urljoin(response.url, url))
        #
        # # Get item URLs and yield Requests
        # item_selector = response.xpath('//*[@itemprop="url"]/@href')
        # for url in item_selector.extract():
        #     yield Request(urljoin(response.url, url), callback=self.parse_item)


    def parse_item(self, response):
        # Create the loader using the response
        l = ItemLoader(item=PropertiesItem(), response=response)

        # Load fields using XPath expressions
        l.add_xpath("title", '//*[@itemprop="name"][1]/text()', MapCompose(str.strip, str.title))
        l.add_xpath("price", '//*[@itemprop="price"][1]/text()', MapCompose(lambda i: i.replace(',',''),float), re='[.0-9]+')
        l.add_xpath("description", '//*[@itemprop="description"][1]/text()', MapCompose(str.strip), Join())
        l.add_xpath("address", '//*[@itemtype="http://schema.org/Place"][1]/text()', MapCompose(str.strip))
        l.add_xpath("image_urls", '//*[@itemprop="image"][1]/@src', MapCompose(lambda i: urljoin(response.url, i)))

        # Housekeeping fields
        l.add_value('url', response.url)
        l.add_value('project', self.settings.get('BOT_NAME'))
        l.add_value('spider', self.name)
        l.add_value('server', socket.gethostname())
        l.add_value('date', datetime.datetime.now())

        l.add_value('title', response.meta['title'], MapCompose(str.strip, str.title))

        return l.load_item()