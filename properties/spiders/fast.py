# -*- coding: utf-8 -*-
import scrapy
from properties.items import PropertiesItem
from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose, Join
from scrapy.http import Request
from urllib.parse import urljoin
import datetime
import socket

class FastSpider(scrapy.Spider):
    name = 'fast'
    allowed_domains = ['web']
    start_urls = ['http://web:9312/properties/index_00000.html']

    def parse(self, response):
        # Get the next index URLs and yield Requests
        next_selector = response.xpath('//*[contains(@class, "next")]//@href')
        for url in next_selector.extract():
            yield Request(urljoin(response.url, url))

        # Get item URLs and yield Requests
        selectors = response.xpath('//*[@itemtype="http://schema.org/Product"]')
        for selector in selectors:
            yield self.parse_item(selector, response)


    def parse_item(self, selector, response):
        # Create the loader using the response
        l = ItemLoader(item=PropertiesItem(), selector=selector)

        # Load fields using XPath expressions
        l.add_xpath("title", './/*[@itemprop="name"][1]/text()', MapCompose(str.strip, str.title))
        l.add_xpath("price", './/*[@itemprop="price"][1]/text()', MapCompose(lambda i: i.replace(',',''),float), re='[.0-9]+')
        l.add_xpath("description", './/*[@itemprop="description"][1]/text()', MapCompose(str.strip), Join())
        l.add_xpath("address", './/*[@itemtype="http://schema.org/Place"][1]/text()', MapCompose(str.strip))
        l.add_xpath("image_urls", './/*[@itemprop="image"][1]/@src', MapCompose(lambda i: urljoin(response.url, i)))

        # Housekeeping fields
        l.add_value('url', './/*[@itemprop="url"][1]/@href', MapCompose(lambda i: urljoin(response.url, i)))
        l.add_value('project', self.settings.get('BOT_NAME'))
        l.add_value('spider', self.name)
        l.add_value('server', socket.gethostname())
        l.add_value('date', datetime.datetime.now())

        return l.load_item()